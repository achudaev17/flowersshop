create database ���������_�����
go

use ���������_�����
go

create table ����_�������
(
ID_���� int primary key identity,
������������ varchar(25) unique  not null
)
go

create table ������
(
ID_������ int primary key identity,
������������ varchar(50) unique not null,
���_������ int foreign key references ����_�������(ID_����) not null,
�����_�� int not null,
������_�� int not null,
������_�� int not null,
�������� varchar(255) not null,
��������� money not null
)
go

create table �����
(
ID_������ int primary key identity,
��������������� int not null
)
go

create table ������_��_������
(
ID_������ int primary key identity,
����� int foreign key references �����(ID_������) not null,
����� int foreign key references ������(ID_������) not null,
���������� int not null
)
go

create table �������������_�����������
(
ID_������������� int primary key identity, 
������� varchar(25) not null,
��� varchar(25) not null,
�������� varchar(25) not null,
������� varchar(20) not null,
)
go

create table ����������
(
ID_���������� int primary key identity,
��������_�������� varchar(50) unique not null,
�����_�������� varchar(255) not null,
����������_���� int foreign key references �������������_�����������(ID_�������������) not null
)
go

create table ��������
(
ID_�������� int primary key identity,
��������� int foreign key references ����������(ID_����������) not null,
����_�������� date not null
)
go

create table ������_�_���������
(
ID_������ int primary key identity,
�������� int foreign key references ��������(ID_��������) not null,
����� int foreign key references ������(ID_������) not null,
���������� int not null
)
go

create table ����_�������������
(
ID_���� int primary key identity,
������������ varchar(25) unique not null 
)
go

create table ������������
(
ID_������������ int primary key identity,
����� varchar(25) unique not null,
������ varchar(25) not null,
���� int foreign key references ����_�������������(ID_����) not null
)
go

create table ����������
(
ID_���������� int primary key identity,
������� varchar(25) not null, 
��� varchar(25) not null, 
�������� varchar(25) not null, 
������� varchar(25) not null,
������_���_����� int foreign key references ������������(ID_������������) not null
)
go

create table �������
(
ID_������� int primary key identity,
������� varchar(25) not null,
��� varchar(25) not null,
������� varchar(25) not null,
������_���_����� int foreign key references ������������(ID_������������) not null
)
go

create table ������
(
ID_������ int primary key identity,
����� int foreign key references ������(ID_������) not null,
���������� int not null,
������_�������� varchar(30) not null,
������_������ varchar(30) not null,
�����_�������� varchar(150) not null,
����_������ date not null,
�����_������ money not null
)
go

create table ������_��������
(
ID_������ int primary key identity,
����� int foreign key references ������(ID_������) not null,
������ int foreign key references �������(ID_�������) not null
)
go